package com.example.jerson.security_house_android;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.drakeet.materialdialog.MaterialDialog;

public class ReportActivity extends AppCompatActivity {
    public static final String TAG = "ReportActivity";
    ListView listView;

    public Messenger mqttServiceMessenger = null;
    public Messenger messageHandler = new Messenger(new MessageHandler());

    IntentFilter intentFilter;
    PushReceiver pushReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "ReportActivity.onCreate");
        setContentView(R.layout.activity_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHome);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(onClickListener);
        listView = (ListView) findViewById(R.id.listReport);

        AttempGetReports task = new AttempGetReports(ReportActivity.this);
        task.userId = SH.UserId;
        task.token = SH.UserAccessToken;
        task.execute();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(ReportActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "ReportActivity.onStart");
        bindService(new Intent(this, MQTTService.class), serviceConnection, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "ReportActivity.onResumen");
        SH.IsAppRunning = true;

        intentFilter = new IntentFilter();
        intentFilter.addAction(SH.MQTT.INTENT_ACTION_MESSAGE);
        intentFilter.addAction(SH.MQTT.INTENT_ACTION_RESPONSE);
        pushReceiver = new PushReceiver();
        registerReceiver(pushReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "ReportActivity.onPause");
        SH.IsAppRunning = false;
        unregisterReceiver(pushReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "ReportActivity.onStop");
        unbindService(serviceConnection);
    }


    class MyAdapter extends ArrayAdapter<Reports>{
        public static final String TAG ="MyAdapter";
        private  ArrayList<Reports> listReports;
        private Context context;

        public MyAdapter(Context ctx, ArrayList<Reports> results){
            super(ctx, R.layout.row, results);
            listReports = results;
            context = ctx;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row, parent, false);

            TextView personaName = (TextView) rowView.findViewById(R.id.userNameReport);
            TextView infoReport = (TextView) rowView.findViewById(R.id.infoReport);

            personaName.setText(listReports.get(position)._id +" : "+listReports.get(position).personName);
            infoReport.setText("Hora: "+listReports.get(position).date.hour+", Fecha: "+
                    listReports.get(position).date.day+"/"+
                    listReports.get(position).date.month+"/"+
                listReports.get(position).date.year);
            return  rowView;
        }
    }

    class AttempGetReports extends GetReportsTask{
        public static final String TAG = "AttempGetReports";
        ProgressDialog progressDialog;
        MaterialDialog dialog;

        public AttempGetReports(Context ctx) {
            super(ctx);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ReportActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_report_loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, HttpResponse<ArrayList>> result) {
            super.onPostExecute(result);
            Log.d(TAG, "Error "+result.data);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if(result.error != null){
                Toast.makeText(ReportActivity.this, "Error type: " + result.error.message, Toast.LENGTH_SHORT).show();
            }else{
                switch (result.data.code){
                    case 200:
                        Log.e(TAG, "todo bien");
                        MyAdapter myAdapter = new MyAdapter(ReportActivity.this, result.data.data);

                        listView.setAdapter(myAdapter);

                        break;
                    default:
                        Toast.makeText(ReportActivity.this, "Algo salio mal", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }


    public class PushReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String topic = intent.getStringExtra(SH.MQTT.KEYS.TOPIC);
            String message = intent.getStringExtra(SH.MQTT.KEYS.MESSAGE);

            Log.e(TAG, "Topic Receiver: " + topic);
            Log.e(TAG, "Message Receiver: " + message);

            try {
                JSONObject jsonResponse = new JSONObject(message);
                String type = jsonResponse.getString("type");
                JSONObject data = jsonResponse.getJSONObject("data");
                AlertDialog.Builder builder = new AlertDialog.Builder(ReportActivity.this);
                if (type.equals("response")) {
                    String messageResponse = data.getString("message");
                    if (messageResponse.equals("success_od")) {
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Ok</font>"))
                                .setMessage("La puerta a sido abierta con  exito")
                                .setIcon(R.drawable.ic_check)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    } else if (messageResponse.equals("failed_od")) {
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Error</font>"))
                                .setMessage("La puerta a fallado al intentar abrir")
                                .setIcon(R.drawable.ic_ex)
                                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                } else if (type.equals("message")) {
                    String reason = data.getString("reason");
                    String person = data.getString("person");
                    if (reason.equals("open_door")) {
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Info</font>"))
                                .setMessage("La puesta a sido abierta por " + person)
                                .setIcon(R.drawable.ic_info_green)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    } else if (reason.equals("attempt_open_door")) {
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Alerta</font>"))
                                .setMessage("Han intentado abrir la puerta por una persona desconocida")
                                .setIcon(R.drawable.ic_info_red)
                                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }

                AlertDialog dialog = builder.create();
                dialog.show();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "HomeActivity.MessageHandler.handleMessage");
            switch (msg.what){
                case SH.MQTT.ACTION.SUBSCRIBE:
                    break;
                case SH.MQTT.ACTION.PUBLISH:
                    Bundle data = msg.getData();
                    Boolean status = data.getBoolean("published");
                    if(status){
                        //algo
                    }else{
                        String error = data.getString("error");
                        Toast.makeText(getApplicationContext(),"El mensaje no pudo ser enviado "+error, Toast.LENGTH_LONG).show();
                    }
            }
            super.handleMessage(msg);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "HomeActivity.ServiceConnection.onServiceConnected");
            mqttServiceMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "HomeActivity.ServiceConnection.onServiceDisconnected");
        }
    };
}
