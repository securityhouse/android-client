package com.example.jerson.security_house_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;


class SignupTask extends AsyncTask<String, Void, Res<Error, Integer>>{
    public static final String TAG = "SignupTask";

    String username, password;
    Context context;

    public SignupTask(Context ctx){
        context = ctx;
    }

    @Override
    protected Res<Error, Integer> doInBackground(String... params) {
        try{
            HTTPClient httpClient = new HTTPClient(SH.API_SERVER_HOST+"/auth/signup");
            String bodyParams = "username="+username+"&password="+password;
            Log.d(TAG, bodyParams);
            httpClient.setMethod("POST");
            httpClient.addParams(bodyParams);
            httpClient.makeRequest();
            String response = httpClient.getStringFromResponse(httpClient.getHttpResponse());
            Log.i(TAG, response);
            int httpResponseCode = httpClient.getConnection().getResponseCode();
            if(httpResponseCode == 201){
                String contentType = httpClient.getConnection().getHeaderField("Content-Type");
                if(contentType.equals("application/json; charset=utf-8") || contentType.equals("application/json") ){
                    JSONObject jsonResponse = new JSONObject(response);
                    JSONObject data = jsonResponse.getJSONObject("data");
                    JSONObject user = data.getJSONObject("user");
                    String token= data.getString("token");
                    String userName = user.getString("username");
                    String userId = user.getString("_id");

                    Log.i(TAG,"Token: "+token);
                    Log.i(TAG,"userName: "+userName);
                    Log.i(TAG,"userId: "+userId);

                    SharedPreferences sp = context.getSharedPreferences(SH.SHARED_PREFERENCES, context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("userName", userName);
                    editor.putString("userAccessToken", token);
                    editor.putString("userId", userId);

                    if(SH.DeviceId != null){
                        editor.putString("userId", "/messages/to"+username+"/android/"+SH.DeviceId);
                    }
                    editor.apply();
                    SH.UserAccessToken = token;
                    SH.UserName =userName;
                    SH.UserId = userId;
                }
            }

            return new Res(null, httpResponseCode);
        }catch (UnknownHostException ukhe){
            return new Res(new Error(500, ukhe.getMessage(), "ukhe"), null);
        }catch (Exception e){
            Log.e(TAG, "Error: "+e.getMessage());
            return new Res(new Error(500, e.getMessage(), "e"), null);
        }
    }
}

class LoginTask extends AsyncTask<String, Void, Res<Error, Integer>>{
    public static final String TAG ="LoginTask";

    String userName;
    String password;
    Context context;

    public LoginTask(Context ctx){
        context =ctx;
    }

    @Override
    protected Res<Error, Integer> doInBackground(String... params) {
        try{
            HTTPClient httpClient = new HTTPClient(SH.API_SERVER_HOST+"/auth/login");
            String bodyParams = "username="+userName+"&password="+password;
            Log.i(TAG, "params "+bodyParams);
            httpClient.setMethod("POST");
            httpClient.addParams(bodyParams);
            httpClient.makeRequest();
            String response = httpClient.getStringFromResponse(httpClient.getHttpResponse());
            int httpResponseCode = httpClient.getConnection().getResponseCode();
            if(httpResponseCode == 200){
                String content = httpClient.getConnection().getHeaderField("Content-type");
                if(content.equals("application/json; charset=utf-8") || content.equals("application/json")){
                    JSONObject jsonResponse = new JSONObject(response);
                    Log.i(TAG,response);
                    JSONObject data = jsonResponse.getJSONObject("data");
                    JSONObject user = data.getJSONObject("user");
                    String token = data.getString("token");
                    String username = user.getString("username");
                    String userId = user.getString("_id");
                    String houseId = user.getString("idHouse");
                    Log.i(TAG, houseId);

                    SharedPreferences sp = context.getSharedPreferences(SH.SHARED_PREFERENCES, context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("userAccessToken", token);
                    editor.putString("userId", userId);
                    editor.putString("username", username);
                    editor.putString("houseId", houseId);
                    editor.apply();

                    SH.UserAccessToken = token;
                    SH.UserName = username;
                    SH.UserId = userId;
                    SH.HouseId = houseId;

                    Log.i(TAG, SH.HouseId);
                }
            }
            return new Res(null, httpResponseCode);
        }catch (UnknownHostException ukhe){
            return new Res(new Error(500, ukhe.getMessage(), "ukhe"), null);
        }catch (Exception e){
            return new Res(new Error(500, e.getMessage(), "e"), null);
        }
    }
}

class GetUsersTask extends AsyncTask<String, Void, Res<Error, HttpResponse<ArrayList>>> {
    public static final String TAG = "GetUsersTask";

    public String token = "";
    private Gson gson = new Gson();
    Context context;

    public GetUsersTask(Context ctx){
        context = ctx;
    }

    @Override
    protected Res<Error, HttpResponse<ArrayList>> doInBackground(String... params) {
        try{
            HTTPClient httpClient = new HTTPClient(SH.API_SERVER_HOST + "/users");
            httpClient.addHeader("Authorization", token);
            httpClient.setMethod("GET");
            httpClient.makeRequest();
            String response = httpClient.getStringFromResponse(httpClient.getHttpResponse());
            int httpResponseCode = httpClient.getConnection().getResponseCode();
            if(httpResponseCode == 200){
                String content = httpClient.getConnection().getHeaderField("Content-Type");
                Log.i(TAG, "equal: "+ content );
                if(content != null){
                    if(content.equals("application/json:charset=utf-8") || content.equals("application/json")){
                        JSONObject jsonResponse = new JSONObject(response);
                        Users[] users = gson.fromJson(jsonResponse.getJSONArray("data").toString(), Users[].class);
                        SH.users = new ArrayList<>(Arrays.asList(users));
                    }
                }else{
                    return new Res(new Error(500, "Content-type is null","null"), null);
                }
            }
            return new Res(null, new HttpResponse(httpResponseCode, SH.users));
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
            return new Res(new Error(500, e.getMessage(), "e"), null);
        }
    }
}

class GetReportsTask extends AsyncTask<String, Void, Res<Error,HttpResponse<ArrayList>>>{
    public static final String TAG = "GetReportsTask";

    public String token = "";
    public String userId;
    private Gson gson = new Gson();
    Context context;

    public GetReportsTask(Context ctx){
        context = ctx;
    }

    @Override
    protected Res<Error,HttpResponse<ArrayList>> doInBackground(String... params) {
        try{
            HTTPClient httpClient = new HTTPClient(SH.API_SERVER_HOST + "/records?userId="+userId);
            httpClient.addHeader("Authorization", "Bearer "+token);
            httpClient.setMethod("GET");
            httpClient.makeRequest();
            String response = httpClient.getStringFromResponse(httpClient.getHttpResponse());
            int httpResponseCode = httpClient.getConnection().getResponseCode();
            if(httpResponseCode != 200) {
                return new Res(null, new HttpResponse(httpResponseCode, null));
            }
            String content = httpClient.getConnection().getHeaderField("Content-Type");
            Log.i(TAG, "equal: "+ content );
            if(content == null &&
                    !content.equals("application/json:charset=utf-8") &&
                    !content.equals("application/json")) {
                return new Res(new Error(500, "Content-type is null", "null"), null);
            }
            JSONObject jsonResponse = new JSONObject(response);
            Reports[] reports = gson.fromJson(jsonResponse.getJSONArray("data").toString(), Reports[].class);
            ArrayList lista = new ArrayList(Arrays.asList(reports));
            return new Res(null, new HttpResponse(httpResponseCode, lista));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return new Res(new Error(500, e.getMessage(), "e"), null);
        }
    }
}
