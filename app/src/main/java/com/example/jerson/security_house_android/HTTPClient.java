package com.example.jerson.security_house_android;


import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class HTTPClient {
    public static final String TAG = "HTTPClient";

    private HttpURLConnection connection;
    private String url;
    private String method;
    private InputStream httpResponse;

    public HTTPClient(String url) throws Exception {
        url = url.replace(" ", "%20");
        URL urlObject = new URL(url);
        this.url = url;
        connection = (HttpURLConnection) urlObject.openConnection();
    }

    public HttpURLConnection getConnection(){
        return this.connection;
    }

    public void setMethod(String method) throws Exception {
        this.method = method;
        this.connection.setRequestMethod(method);

        if(this.method != "GET"){
            this.connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        }

        this.connection.setRequestProperty("Content-Length","0");
        this.connection.setUseCaches(false);
        this.connection.setAllowUserInteraction(false);
    }

    public String getMethod(){
        return this.method;
    }

    public void addHeader(String key, String value) throws Exception{
        this.connection.setRequestProperty(key, value);
    }

    public void addParams(String params) throws Exception {
        byte[] data = params.getBytes(Charset.forName("UTF-8"));
        int dataLength = data.length;
        this.connection.setRequestProperty("Content-Length", Integer.toString(dataLength));
        DataOutputStream wr = new DataOutputStream(this.connection.getOutputStream());
        wr.write(data);
    }

    public void makeRequest() throws Exception {
        Log.i(TAG, "Making request to " + this.url);
        this.connection.connect();

        int responseCode = this.connection.getResponseCode();
        String contentType = this.connection.getContentType();

        Log.i(TAG, "HttpResponse code: "+responseCode);
        Log.i(TAG, "Content type: " + contentType);
        Log.i(TAG, "res: "+this.isErrorCode(responseCode));

        if(this.isErrorCode(responseCode)){
            this.httpResponse = this.connection.getErrorStream();
        }else {
            this.httpResponse = this.connection.getInputStream();
        }
    }

    public boolean isErrorCode(int errorCode){
        if(errorCode >= 400){
            return true;
        }else{
            return false;
        }
    }

    public InputStream getHttpResponse(){
        return this.httpResponse;
    }

    public static String getStringFromResponse(InputStream stream){
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuffer sb = new StringBuffer();
        String line = null;
        try{
            while ((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try{
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
