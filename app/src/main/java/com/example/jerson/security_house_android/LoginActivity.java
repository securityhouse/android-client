package com.example.jerson.security_house_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.drakeet.materialdialog.MaterialDialog;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = "LoginActivity";

    private EditText edt1;
    private EditText edt2;
    private TextView buttonSingup;
    private Button buttonLogin;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edt1 = (EditText) findViewById(R.id.username_login);
        edt2 = (EditText) findViewById(R.id.password_login);

        //buttonSingup = (TextView) findViewById(R.id.bSingup);
        //buttonSingup.setOnClickListener(onClickSingupButton);

        buttonLogin = (Button) findViewById(R.id.bLogin);
        buttonLogin.setOnClickListener(onClickLoginButton);

        sharedPreferences = getSharedPreferences(SH.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SH.UserAccessToken = sharedPreferences.getString("userAccessToken", "");
        SH.UserName = sharedPreferences.getString("userName", "");
        SH.UserId = sharedPreferences.getString("userId", "");
        SH.DeviceId = sharedPreferences.getString("deviceId", "");
        SH.HouseId = sharedPreferences.getString("homeId", "");

        Log.i(TAG, "Token: "+SH.UserAccessToken);
        Log.i(TAG, "UserName: "+SH.UserName);
        Log.i(TAG, "UserId: "+SH.UserId);
        Log.i(TAG, "DeviceId: "+SH.DeviceId);
        Log.i(TAG, "HomeId: "+SH.HouseId);

        if(SH.DeviceId.isEmpty()){
            Log.i(TAG, "Getting deviceId from setting");
            String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            SH.DeviceId = deviceId;
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("deviceId", deviceId);
            editor.apply();
            Log.i(TAG, "DeviceId Created: "+SH.DeviceId);
        }

        if(SH.UserAccessToken != ""){
            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }
    }

    private View.OnClickListener onClickSingupButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "Button register clicked");
            Intent i = new Intent(LoginActivity.this, SingupActivity.class);
            startActivity(i);
            finish();
        }
    };

    private  View.OnClickListener onClickLoginButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "Button Login clicked");
            String userName = edt1.getText().toString();
            String password = edt2.getText().toString();
            if(!userName.isEmpty() && !password.isEmpty()){
                Log.d(TAG, "no vacia");
                AttempLogin task = new AttempLogin(LoginActivity.this);
                task.userName = userName;
                task.password = password;
                task.execute();
            }else{
                Toast.makeText(LoginActivity.this, R.string.phrase_login_emptyCredentials, Toast.LENGTH_LONG).show();
            }
        }
    };

    public class AttempLogin extends LoginTask{
        public static final String TAG = "AttempLogin";
        ProgressDialog progressDialog;
        MaterialDialog dialog;


        public AttempLogin(Context ctx){
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_login_loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error, Integer> result) {
            super.onPostExecute(result);
            Log.d(TAG, "Error "+result.data);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if(result.error != null){
                Toast.makeText(LoginActivity.this, "Error type: " + result.error.message, Toast.LENGTH_SHORT).show();
            }else{
                switch (result.data){
                    case 200:
                        Log.i(TAG, "Token: "+SH.UserAccessToken);
                        Log.i(TAG, "UserName: "+SH.UserName);
                        Log.i(TAG, "UserId: "+SH.UserId);

                        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                        break;
                    case 401:
                       dialog = new MaterialDialog(LoginActivity.this);
                        dialog.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Alert</font>"))
                                .setMessage(R.string.phrase_login_wrongUser)
                                .setPositiveButton("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                        dialog.show();
                        break;
                    default:
                        Toast.makeText(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
