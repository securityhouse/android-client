package com.example.jerson.security_house_android;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeActivity extends AppCompatActivity {
    public static final String TAG = "HomeActivity";

    public Messenger mqttServiceMessenger = null;
    public Messenger messageHandler = new Messenger(new MessageHandler());

    IntentFilter intentFilter;
    PushReceiver pushReceiver;

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHome);
        setSupportActionBar(toolbar);

        sp = getSharedPreferences(SH.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SH.UserAccessToken = sp.getString("userAccessToken", "");
        SH.UserName = sp.getString("userName", "");
        SH.UserName = sp.getString("userId", "");
        SH.HouseId = sp.getString("houseId", "");

        Log.i(TAG, "Token: "+SH.UserAccessToken);
        Log.i(TAG, "UserName: "+SH.UserName);
        Log.i(TAG, "UserId: "+SH.UserId);
        Log.i(TAG, "houseId: "+SH.HouseId);

        startService(new Intent(this, MQTTService.class));

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            //falta por implementar
            Log.i(TAG, "entro aqui");

            String topic = bundle.getString(SH.MQTT.KEYS.TOPIC,"");
            String message = bundle.getString(SH.MQTT.KEYS.MESSAGE,"");

            Log.i(TAG, "Topic: "+topic);
            Log.i(TAG, "Message: "+message);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "HomeActivity.onStart");
        bindService(new Intent(this, MQTTService.class), serviceConnection, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "HomeActivity.onResumen");
        SH.IsAppRunning = true;

        intentFilter = new IntentFilter();
        intentFilter.addAction(SH.MQTT.INTENT_ACTION_MESSAGE);
        intentFilter.addAction(SH.MQTT.INTENT_ACTION_RESPONSE);
        pushReceiver = new PushReceiver();
        registerReceiver(pushReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "HomeActivity.onPause");
        SH.IsAppRunning = false;
        unregisterReceiver(pushReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "HomeActivity.onStop");
        unbindService(serviceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()){
            case R.id.HomeItemReport:
                i = new Intent(HomeActivity.this, ReportActivity.class);
                startActivity(i);
                return true;
            case R.id.HomeItemLogout:
                SH.UserAccessToken = "";
                SH.UserName = "";
                SH.UserName = "";

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("userAccessToken", "");
                editor.putString("userName", "");
                editor.putString("userId", "");
                editor.apply();

                stopService(new Intent(this, MQTTService.class));

                i = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void click(View view) {
        if (mqttServiceMessenger != null) {
            Message message = Message.obtain(null, SH.MQTT.ACTION.PUBLISH);

            String mqttTopic = "/securityhouse/to/" + SH.UserId + "/house/"+SH.HouseId;
            String replyTo = "/securityhouse/to/" + SH.UserId + "/android/" + SH.DeviceId;
            JSONObject json = new JSONObject();
            try {
                json.put("replyTo", replyTo);
                json.put("type", "od");
            } catch (Exception e) {
                Log.e(TAG, "aqui llego");
                e.printStackTrace();
            }

            Bundle data = new Bundle();

            data.putString(SH.MQTT.KEYS.TOPIC, mqttTopic);
            data.putString(SH.MQTT.KEYS.MESSAGE, json.toString());
            message.setData(data);
            message.replyTo = messageHandler;
            Log.i(TAG, "Sending message to publish");
            try {
                mqttServiceMessenger.send(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "MotoStatusActivity.mqttServiceMessenger is null");
        }
    }

    public void showCamera(View view) {
        try {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.rcreations.ipcamviewerBasic");
            startActivity(launchIntent);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    class MessageHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "HomeActivity.MessageHandler.handleMessage");
            switch (msg.what){
                case SH.MQTT.ACTION.SUBSCRIBE:
                    break;
                case SH.MQTT.ACTION.PUBLISH:
                    Bundle data = msg.getData();
                    Boolean status = data.getBoolean("published");
                    if(status){
                        //algo
                    }else{
                        String error = data.getString("error");
                        Toast.makeText(getApplicationContext(),"El mensaje no pudo ser enviado "+error, Toast.LENGTH_LONG).show();
                    }
            }
            super.handleMessage(msg);
        }
    }

    public class PushReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String topic = intent.getStringExtra(SH.MQTT.KEYS.TOPIC);
            String message = intent.getStringExtra(SH.MQTT.KEYS.MESSAGE);

            Log.e(TAG, "Topic Receiver: "+topic);
            Log.e(TAG, "Message Receiver: "+message);

            try {
                JSONObject jsonResponse = new JSONObject(message);
                String type = jsonResponse.getString("type");
                JSONObject data = jsonResponse.getJSONObject("data");
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                if(type.equals("response")){
                    String messageResponse = data.getString("message");
                    if(messageResponse.equals("success_od")){
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Ok</font>"))
                                .setMessage("La puerta a sido abierta con  exito")
                                .setIcon(R.drawable.ic_check)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    }else if(messageResponse.equals("failed_od")){
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Error</font>"))
                                .setMessage("La puerta a fallado al intentar abrir")
                                .setIcon(R.drawable.ic_ex)
                                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }else if(type.equals("message")){
                    String reason = data.getString("reason");
                    String person = data.getString("person");
                    if(reason.equals("open_door")){
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Info</font>"))
                                .setMessage("La puesta a sido abierta por "+person)
                                .setIcon(R.drawable.ic_info_green)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                    }else if(reason.equals("attempt_open_door")){
                        builder.setTitle(Html.fromHtml("<font color='" + R.color.primary_dark + "'>Alerta</font>"))
                                .setMessage("Han intentado abrir la puerta por una persona desconocida")
                                .setIcon(R.drawable.ic_info_red)
                                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }

                AlertDialog dialog = builder.create();
                dialog.show();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "HomeActivity.ServiceConnection.onServiceConnected");
            mqttServiceMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "HomeActivity.ServiceConnection.onServiceDisconnected");
        }
    };
}
