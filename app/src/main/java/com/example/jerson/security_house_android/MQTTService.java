package com.example.jerson.security_house_android;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import java.util.Arrays;


public class MQTTService extends Service {

    public static final String TAG = "MQTTService";

    private int connectionState = SH.MQTT.CONNECTION_STATE.DISCONNECTED;
    private  static boolean serviceIsRunning = false;
    private static MQTTConnection connection = null;

    private Messenger messageHandler = new Messenger(new MessageHandler());

    private String deviceId;
    private String  userId;
    private String houseId;

    @Override
    public void onCreate() {
        Log.i(TAG, "MQTTService.onCreate");
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "MQTTService.onBind");
        return messageHandler.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "MQTTService.onStartCommand");
        if(serviceIsRunning){
            Log.i(TAG, "Ya esta corriendo");

            return START_STICKY;
        }else{
            Log.i(TAG, "No esta corriendo");

            super.onStartCommand(intent, flags, startId);
            SharedPreferences sp = getSharedPreferences(SH.SHARED_PREFERENCES, Context.MODE_PRIVATE);

            deviceId = sp.getString("deviceId", "");
            userId = sp.getString("userId", "");
            houseId = sp.getString("houseId", "");

            if(!deviceId.isEmpty() && !userId.isEmpty() && !houseId.isEmpty()){
                connection = new MQTTConnection();
            }else{
                Log.i(TAG, "Empty credentials");
            }
            if(connection != null){

                Log.i(TAG, "Inicio de conexion");
                connection.start();
                serviceIsRunning = true;
            }
            return START_STICKY;
        }
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "MQTTService.onDestroy");

        if(connection != null){
            connection.end();
        }
        SH.users = null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "MQTTService.onUnbind");
        return super.onUnbind(intent);
    }

    class MessageHandler extends Handler{
        public static final String TAG = "MessageHandler";
        @Override
        public void handleMessage(Message message) {
            Log.i(TAG, "MQTTService.MessageHandler.handleMessage");
            if(connection != null){
                switch (message.what){
                    case SH.MQTT.ACTION.CONNECT:
                        connection.connect();
                        break;
                    case SH.MQTT.ACTION.SUBSCRIBE:
                        connection.subscribe(message);
                        break;
                    case SH.MQTT.ACTION.UNSUBSCRIBE:
                        connection.unsubscribe(message);
                        break;
                    case SH.MQTT.ACTION.PUBLISH:
                        connection.publish(message);
                        break;
                }
            }else{
                if(message.replyTo != null){
                    Bundle data = new Bundle();
                    data.putString("error","null_connection");
                    Message m = Message.obtain(null, message.what);
                    m.setData(data);
                    try{
                        message.replyTo.send(m);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class MQTTConnection extends Thread implements MqttCallback{
        public static final String TAG = "MQTTConnection";

        private int timeout = 5000;
        private String uri;
        private MqttClient client = null;
        private MqttConnectOptions options = new MqttConnectOptions();
        private MessageHandler MQTTMessageHandler = new MessageHandler();

        MQTTConnection(){
            uri = "tcp://"+ SH.MQTT.SERVER_HOST + ":"+SH.MQTT.SERVER_PORT;
            options.setCleanSession(false);
            try{
                Log.i(TAG, "Creating mqttClient");
                client = new MqttClient(uri, deviceId, null);
                client.setCallback(this);
                this.connect();
            } catch (MqttException e) {
                e.printStackTrace();
                Log.e(TAG, e.getMessage());
            }
        }

        public void end(){
            client.setCallback(this);
            if(client.isConnected()){
                try {
                    client.disconnect();
                    client.close();
                } catch (MqttException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }
            }
        }

        public void connect(){
            if(connectionState != SH.MQTT.CONNECTION_STATE.CONNECTED){
                try {
                    Log.e(TAG, "Trying to connect to"+ uri);
                    client.connect(options);
                    connectionState = SH.MQTT.CONNECTION_STATE.CONNECTED;
                    Log.i(TAG, "Connected to "+uri);

                    subscribe("/securityhouse/to/"+userId+"/android/"+deviceId, 1);
                    subscribe("/securityhouse/from/"+userId+"/house/"+houseId, 1);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }

        private void delayReconnect(){
            MQTTMessageHandler.sendEmptyMessageDelayed(SH.MQTT.ACTION.CONNECT, timeout);
        }

        private void subscribe(Message message){
            Bundle response = new Bundle();
            Bundle data = message.getData();
            if(data != null){
                String topic = data.getString(SH.MQTT.KEYS.TOPIC);
                int qos = data.getInt(SH.MQTT.KEYS.QOS);
                if(topic != null && !topic.isEmpty()){
                    response = subscribe(topic, qos);
                }
            }
            if(message.replyTo != null){
                Message m = Message.obtain(null, message.what);
                message.replyTo = messageHandler;
                m.setData(response);
                try{
                    message.replyTo.send(m);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        private Bundle subscribe(String topic, int qos){
            Bundle response = new Bundle();
            try{
                Log.i(TAG, "Triying to subscribe to "+topic);
                client.subscribe(topic, qos);
                Log.i(TAG, "Subscribed to "+topic);
                response.putBoolean("subscribed", true);
            } catch (MqttException e) {
                Log.d(TAG, "Subscribe failed with reason code = " + e.getReasonCode());
                response.putBoolean("subscribed", false);
                response.putString("error", e.getMessage());
            }
            return response;
        }

        private Bundle subscribe(String[] topics, int[] qos){
            Bundle data = new Bundle();
            try{
                Log.i(TAG, "Trying to subscribe to " + Arrays.toString(topics));
                client.subscribe(topics, qos);
                Log.i(TAG, "Subscribed to " + Arrays.toString(topics));
                data.putBoolean("subscribed", true);
            } catch (MqttException e) {
                Log.d(TAG, "Subscribe failed with reason code = " + e.getReasonCode());
                data.putBoolean("subscribed", false);
                data.putString("error", e.getMessage());
            }
            return data;
        }

        private void unsubscribe(Message message){
            Bundle response = new Bundle();
            Bundle data = message.getData();
            if(data != null){
                String topic = data.getString(SH.MQTT.KEYS.TOPIC);
                if(topic != null && !topic.isEmpty()){
                    response = unsubscribe(topic);
                }
            }

            if(message.replyTo != null) {
                Message m = Message.obtain(null, message.what);
                message.replyTo = messageHandler;
                m.setData(response);
                try {
                    message.replyTo.send(m);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private  Bundle unsubscribe(String topic){
            Bundle response = new Bundle();
            try{
                Log.i(TAG, "Trying to unsubscribe to " + topic);
                client.unsubscribe(topic);
                Log.i(TAG, "Unsubscribed to " + topic);
                response.putBoolean("unsubscribed", true);
            } catch (MqttException e) {
                Log.d(TAG, "Unsubscribe failed with reason code = " + e.getReasonCode());
                response.putBoolean("unsubscribed", false);
                response.putString("error", e.getMessage());
            }
            return  response;
        }

        private Bundle unsubscribe(String[] topics){
            Bundle response = new Bundle();
            try{
                Log.i(TAG, "Trying to unsubscribe to " + Arrays.toString(topics));
                client.unsubscribe(topics);
                Log.i(TAG, "Unsubscribed to " + Arrays.toString(topics));
                response.putBoolean("unsubscribed", true);
            } catch (MqttException e) {
                Log.d(TAG, "Subscribe failed with reason code = " + e.getReasonCode());
                response.putBoolean("unsubscribed", false);
                response.putString("error", e.getMessage());
            }
            return response;
        }

        private void publish(Message message){
            Bundle response = new Bundle();
            Bundle data = message.getData();
            if(data != null){
                String topic = data.getString(SH.MQTT.KEYS.TOPIC);
                if(topic != null && !topic.isEmpty()){
                    String m = data.getString(SH.MQTT.KEYS.MESSAGE);
                    int qos = data.getInt(SH.MQTT.KEYS.QOS);
                    if(m != null && !m.isEmpty()){
                        response = publish(topic, m, qos);
                    }
                }
            }
            if(message.replyTo != null){
                Message m = Message.obtain(null, message.what);
                m.replyTo = messageHandler;
                m.setData(response);
                try{
                    message.replyTo.send(m);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        private  Bundle publish(String topic, String message, int qos){
            Bundle response = new Bundle();
            try{
                MqttMessage mqttMessage = new MqttMessage();
                mqttMessage.setPayload(message.getBytes());
                Log.i(TAG, "Trying to publish "+message+" to " + topic);
                client.publish(topic, mqttMessage.getPayload(), qos, false);
                Log.i(TAG, "Message published");
                response.putBoolean("published", true);
            } catch (MqttException e) {
                Log.d(TAG, "Publish failed with reason code = " + e.getReasonCode());
                response.putBoolean("published", false);
                response.putString("error", e.getMessage());
            }
            return response;
        }

        @Override
        public void connectionLost(Throwable throwable) {
            Log.i(TAG, "Connection lost");
            Log.i(TAG, throwable.getMessage());
            connectionState = SH.MQTT.CONNECTION_STATE.DISCONNECTED;
            delayReconnect();
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            Log.i(TAG, topic + ":" + message.toString());
            try{
                JSONObject jsonMessage = new JSONObject(message.toString());

                String type = jsonMessage.getString("type");

                Log.d(TAG, "******************************");
                Log.d(TAG, ""+ SH.IsAppRunning);
                Log.d(TAG, "******************************");

                if(SH.IsAppRunning){
                    Log.d(TAG, "app corriendo");
                    Intent intent = new Intent();
                    if(type.equals("response")){
                        intent.setAction(SH.MQTT.INTENT_ACTION_RESPONSE);
                    }else if(type.equals("message")){
                        intent.setAction(SH.MQTT.INTENT_ACTION_MESSAGE);
                    }

                    intent.putExtra(SH.MQTT.KEYS.TOPIC, topic);
                    intent.putExtra(SH.MQTT.KEYS.MESSAGE, message.toString());
                    sendBroadcast(intent);
                }else{

                    Log.d(TAG, "app no esta corriendo");
                    JSONObject data = new JSONObject(jsonMessage.getString("data"));
                    //String userId = data.getString("id");
                    String reason = data.getString("reason");
                    String idHouse ="";

                    Log.i(TAG, type);
                    int notificationId;
                    if(type.equals("response")){
                        notificationId = SH.MQTT.KEYS.RESPONSE_NOTIFICATION_ID;
                    }else{
                        notificationId = SH.MQTT.KEYS.MESSAGE_NOTIFICATION_ID;
                        idHouse = data.getString("houseId");
                    }

                    Context context = getBaseContext();
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
                    notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
                    Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification_white);
                    int smallIcon = R.drawable.ic_notification_white;
                    String contentTitle = "Nueva Notificacion";
                    String contextText = "Texto de la notificacion";
                    Intent resultIntent =  new Intent(context, HomeActivity.class);

                    boolean showNotification = true;


                    if(reason.equals("failed_od")){
                        Log.i(TAG, "Failed open door");
                        contentTitle = "Respuesta: Error";
                        contextText = "Ocurrio un error al abrir la puerta";
                        largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_ex_red);
                        smallIcon = R.drawable.ic_ex_red;
                    }else if(reason.equals("attempt_open_door")){
                        //fa : failed attempt
                        Log.i(TAG, "Failed attempt");
                        contentTitle = "Respuesta: Intento fallido";
                        contextText = "Han intententado abrir la puerta";
                        largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_info_red);
                        smallIcon = R.drawable.ic_info_red;
                    }else if(reason.equals("open_door")){
                        Log.i(TAG, "Open door success");
                        contentTitle = "Respuesta: Puesta abierta";
                        contextText = "La puesta de la casa fue abierta";
                        largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_info_green);
                        smallIcon = R.drawable.ic_info_green;
                    }else if(reason.equals("success_od")){

                        Log.i(TAG, "Open door success");
                        contentTitle = "Respuesta: Puesta abierta";
                        contextText = "La puerta a sido abierta con  exito";
                        largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_check_green);
                        smallIcon = R.drawable.ic_check_green;
                    }

                    if(showNotification){
                        Log.d(TAG, "Mostrando notificacion");
                        notificationBuilder.setSmallIcon(smallIcon);
                        notificationBuilder.setLargeIcon(largeIcon);
                        notificationBuilder.setContentTitle(contentTitle);
                        notificationBuilder.setContentText(contextText);
                        notificationBuilder.setColor(getResources().getColor(R.color.accent));
                        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(contextText));
                        notificationBuilder.setAutoCancel(true);
                        resultIntent.putExtra("userId", userId);
                        resultIntent.putExtra("type", type);

                        if(idHouse != ""){
                            resultIntent.putExtra("idHouse", idHouse);
                        }

                        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        notificationBuilder.setContentIntent(resultPendingIntent);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(notificationId, notificationBuilder.build());
                    }
                }
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        }
    }
}
