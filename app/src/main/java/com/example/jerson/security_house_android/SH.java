package com.example.jerson.security_house_android;

import java.util.ArrayList;

public class SH {
    //url api
    public static final String API_SERVER_HOST = "http://securityhouse.ssg.company";

    //url shared preferences
    public static final String SHARED_PREFERENCES = "company.ssg.healthyfoodapp.sharedPrefences";

    public static String UserName = "";
    public static String UserId = "";
    public static String HouseId = "";
    public static String UserAccessToken = "";

    public static String DeviceId = "";

    public static ArrayList<Users> users = null;

    public static boolean IsAppRunning = false;

    public static class MQTT{
        public static final String SERVER_HOST = "test.mosquitto.org";
        public static final String SERVER_PORT = "1883";

        public static final String INTENT_ACTION_RESPONSE = "com.example.jerson.security_house_android.mqtt.homeResponse";
        public static final String INTENT_ACTION_MESSAGE = "com.example.jerson.security_house_android.mqtt.homeMessage";



        public static class ACTION{
            public static final int CONNECT = 0;
            public static final int SUBSCRIBE = 1;
            public static final int UNSUBSCRIBE = 2;
            public static final int PUBLISH = 3;
        }

        public static class  CONNECTION_STATE{
            public static final int DISCONNECTED = 0;
            public static final int CONNECTING = 1;
            public static final int CONNECTED = 2;
        }

        public static class KEYS {
            public static final String TOPIC ="topic";
            public static final String MESSAGE = "message";
            public static final String QOS = "qos";

            public static final int RESPONSE_NOTIFICATION_ID = 0;
            public static final int MESSAGE_NOTIFICATION_ID = 1;
        }
    }
}

class Users{
    public int _id;
    public String username;
    public String idHouse;
}

class Reports{
    public int _id;
    public String personName;
    public int userId;
    public Date date;

    class Date{
        public int hour;
        public int  month;
        public int year;
        public int day;
    }
}

class Res<E,D>{
    public E error = null;
    public D data = null;

    public Res(E e, D d){
        error = e;
        data = d;
    }
}

class Error {
    public int code = 0;
    public String message = "";
    public String error = "";

    public Error(int c, String m, String e){
        code = c;
        message = m;
        error = e;
    }
}

class HttpResponse<D>{
    int code = 0;
    D data = null;

    public HttpResponse(int c, D d){
        code = c;
        data = d;
    }
}