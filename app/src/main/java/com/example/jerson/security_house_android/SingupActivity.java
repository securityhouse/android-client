package com.example.jerson.security_house_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SingupActivity extends AppCompatActivity {

    public static final String TAG = "SingupActivity";

    TextView buttonHome;
    Button buttonSingup;
    EditText edt1;
    EditText edt2;
    EditText edt3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        edt1 = (EditText) findViewById(R.id.userName);
        edt2 = (EditText) findViewById(R.id.password);
        edt3 = (EditText) findViewById(R.id.repeatPassword);


        buttonHome = (TextView) findViewById(R.id.buttonHome);
        buttonHome.setOnClickListener(onClickButtonHome);

        buttonSingup = (Button) findViewById(R.id.buttonSingup);
        buttonSingup.setOnClickListener(OnclickButtonSingup);


        Log.i(TAG, "Token: "+SH.UserAccessToken);
        Log.i(TAG, "UserName: "+SH.UserName);
        Log.i(TAG, "UserId: "+SH.UserId);
        Log.i(TAG, "DeviceId: "+SH.DeviceId);
        Log.i(TAG, "HouseId: "+SH.HouseId);

        if(SH.UserAccessToken != ""){
            //Cuando ya este logeado mostrar el Home
            Intent i = new Intent(SingupActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }
    }

    public View.OnClickListener onClickButtonHome = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "Button Home Clicked");
            Intent i = new Intent(SingupActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    };

    public View.OnClickListener OnclickButtonSingup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "Button register Clicked");
            String userName = edt1.getText().toString();
            String password = edt2.getText().toString();
            String repeatPassword = edt3.getText().toString();
            if(!userName.isEmpty() && !password.isEmpty() && !repeatPassword.isEmpty()){
                if(password.equals(repeatPassword)){
                    AttemptSignup task = new AttemptSignup(SingupActivity.this);
                    task.username = userName;
                    task.password = password;
                    task.execute();
                }else{
                    Toast.makeText(SingupActivity.this, R.string.phrase_signup_passwordsNotMach, Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(SingupActivity.this, R.string.phrase_signup_emptyCredentials, Toast.LENGTH_LONG).show();
            }
        }
    };

    public class AttemptSignup extends SignupTask {

        ProgressDialog progressDialog;

        public AttemptSignup(Context ctx){
            super(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SingupActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getString(R.string.phrase_SignUp_registering));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Res<Error,Integer> result) {
            super.onPostExecute(result);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if (result.error != null){
                Toast.makeText(SingupActivity.this, "Error type: "+result.error.message, Toast.LENGTH_SHORT).show();
            }else{
                switch (result.data){
                    case 201:

                        Log.i(TAG, "Token: "+SH.UserAccessToken);
                        Log.i(TAG, "UserName: "+SH.UserName);
                        Log.i(TAG, "UserId: "+SH.UserId);

                        Intent i = new Intent(SingupActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                        break;
                    case 409:
                        Toast.makeText(SingupActivity.this, "Usuario ya existe ", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(SingupActivity.this, "Algo salio mal", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }
}
